#ifndef INCLUDED_CYFITTERIAR_INC
#define INCLUDED_CYFITTERIAR_INC
    INCLUDE cydeviceiar.inc
    INCLUDE cydeviceiar_trm.inc

/* sw1 */
sw1__0__INTTYPE EQU CYREG_PICU12_INTTYPE5
sw1__0__MASK EQU 0x20
sw1__0__PC EQU CYREG_PRT12_PC5
sw1__0__PORT EQU 12
sw1__0__SHIFT EQU 5
sw1__AG EQU CYREG_PRT12_AG
sw1__BIE EQU CYREG_PRT12_BIE
sw1__BIT_MASK EQU CYREG_PRT12_BIT_MASK
sw1__BYP EQU CYREG_PRT12_BYP
sw1__DM0 EQU CYREG_PRT12_DM0
sw1__DM1 EQU CYREG_PRT12_DM1
sw1__DM2 EQU CYREG_PRT12_DM2
sw1__DR EQU CYREG_PRT12_DR
sw1__INP_DIS EQU CYREG_PRT12_INP_DIS
sw1__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU12_BASE
sw1__MASK EQU 0x20
sw1__PORT EQU 12
sw1__PRT EQU CYREG_PRT12_PRT
sw1__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT12_DBL_SYNC_IN
sw1__PRTDSI__OE_SEL0 EQU CYREG_PRT12_OE_SEL0
sw1__PRTDSI__OE_SEL1 EQU CYREG_PRT12_OE_SEL1
sw1__PRTDSI__OUT_SEL0 EQU CYREG_PRT12_OUT_SEL0
sw1__PRTDSI__OUT_SEL1 EQU CYREG_PRT12_OUT_SEL1
sw1__PRTDSI__SYNC_OUT EQU CYREG_PRT12_SYNC_OUT
sw1__PS EQU CYREG_PRT12_PS
sw1__SHIFT EQU 5
sw1__SIO_CFG EQU CYREG_PRT12_SIO_CFG
sw1__SIO_DIFF EQU CYREG_PRT12_SIO_DIFF
sw1__SIO_HYST_EN EQU CYREG_PRT12_SIO_HYST_EN
sw1__SIO_REG_HIFREQ EQU CYREG_PRT12_SIO_REG_HIFREQ
sw1__SLW EQU CYREG_PRT12_SLW

/* sw2 */
sw2__0__INTTYPE EQU CYREG_PICU6_INTTYPE4
sw2__0__MASK EQU 0x10
sw2__0__PC EQU CYREG_PRT6_PC4
sw2__0__PORT EQU 6
sw2__0__SHIFT EQU 4
sw2__AG EQU CYREG_PRT6_AG
sw2__AMUX EQU CYREG_PRT6_AMUX
sw2__BIE EQU CYREG_PRT6_BIE
sw2__BIT_MASK EQU CYREG_PRT6_BIT_MASK
sw2__BYP EQU CYREG_PRT6_BYP
sw2__CTL EQU CYREG_PRT6_CTL
sw2__DM0 EQU CYREG_PRT6_DM0
sw2__DM1 EQU CYREG_PRT6_DM1
sw2__DM2 EQU CYREG_PRT6_DM2
sw2__DR EQU CYREG_PRT6_DR
sw2__INP_DIS EQU CYREG_PRT6_INP_DIS
sw2__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU6_BASE
sw2__LCD_COM_SEG EQU CYREG_PRT6_LCD_COM_SEG
sw2__LCD_EN EQU CYREG_PRT6_LCD_EN
sw2__MASK EQU 0x10
sw2__PORT EQU 6
sw2__PRT EQU CYREG_PRT6_PRT
sw2__PRTDSI__CAPS_SEL EQU CYREG_PRT6_CAPS_SEL
sw2__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT6_DBL_SYNC_IN
sw2__PRTDSI__OE_SEL0 EQU CYREG_PRT6_OE_SEL0
sw2__PRTDSI__OE_SEL1 EQU CYREG_PRT6_OE_SEL1
sw2__PRTDSI__OUT_SEL0 EQU CYREG_PRT6_OUT_SEL0
sw2__PRTDSI__OUT_SEL1 EQU CYREG_PRT6_OUT_SEL1
sw2__PRTDSI__SYNC_OUT EQU CYREG_PRT6_SYNC_OUT
sw2__PS EQU CYREG_PRT6_PS
sw2__SHIFT EQU 4
sw2__SLW EQU CYREG_PRT6_SLW

/* sw3 */
sw3__0__INTTYPE EQU CYREG_PICU6_INTTYPE5
sw3__0__MASK EQU 0x20
sw3__0__PC EQU CYREG_PRT6_PC5
sw3__0__PORT EQU 6
sw3__0__SHIFT EQU 5
sw3__AG EQU CYREG_PRT6_AG
sw3__AMUX EQU CYREG_PRT6_AMUX
sw3__BIE EQU CYREG_PRT6_BIE
sw3__BIT_MASK EQU CYREG_PRT6_BIT_MASK
sw3__BYP EQU CYREG_PRT6_BYP
sw3__CTL EQU CYREG_PRT6_CTL
sw3__DM0 EQU CYREG_PRT6_DM0
sw3__DM1 EQU CYREG_PRT6_DM1
sw3__DM2 EQU CYREG_PRT6_DM2
sw3__DR EQU CYREG_PRT6_DR
sw3__INP_DIS EQU CYREG_PRT6_INP_DIS
sw3__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU6_BASE
sw3__LCD_COM_SEG EQU CYREG_PRT6_LCD_COM_SEG
sw3__LCD_EN EQU CYREG_PRT6_LCD_EN
sw3__MASK EQU 0x20
sw3__PORT EQU 6
sw3__PRT EQU CYREG_PRT6_PRT
sw3__PRTDSI__CAPS_SEL EQU CYREG_PRT6_CAPS_SEL
sw3__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT6_DBL_SYNC_IN
sw3__PRTDSI__OE_SEL0 EQU CYREG_PRT6_OE_SEL0
sw3__PRTDSI__OE_SEL1 EQU CYREG_PRT6_OE_SEL1
sw3__PRTDSI__OUT_SEL0 EQU CYREG_PRT6_OUT_SEL0
sw3__PRTDSI__OUT_SEL1 EQU CYREG_PRT6_OUT_SEL1
sw3__PRTDSI__SYNC_OUT EQU CYREG_PRT6_SYNC_OUT
sw3__PS EQU CYREG_PRT6_PS
sw3__SHIFT EQU 5
sw3__SLW EQU CYREG_PRT6_SLW

/* Buzzer */
Buzzer__0__INTTYPE EQU CYREG_PICU2_INTTYPE6
Buzzer__0__MASK EQU 0x40
Buzzer__0__PC EQU CYREG_PRT2_PC6
Buzzer__0__PORT EQU 2
Buzzer__0__SHIFT EQU 6
Buzzer__AG EQU CYREG_PRT2_AG
Buzzer__AMUX EQU CYREG_PRT2_AMUX
Buzzer__BIE EQU CYREG_PRT2_BIE
Buzzer__BIT_MASK EQU CYREG_PRT2_BIT_MASK
Buzzer__BYP EQU CYREG_PRT2_BYP
Buzzer__CTL EQU CYREG_PRT2_CTL
Buzzer__DM0 EQU CYREG_PRT2_DM0
Buzzer__DM1 EQU CYREG_PRT2_DM1
Buzzer__DM2 EQU CYREG_PRT2_DM2
Buzzer__DR EQU CYREG_PRT2_DR
Buzzer__INP_DIS EQU CYREG_PRT2_INP_DIS
Buzzer__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU2_BASE
Buzzer__LCD_COM_SEG EQU CYREG_PRT2_LCD_COM_SEG
Buzzer__LCD_EN EQU CYREG_PRT2_LCD_EN
Buzzer__MASK EQU 0x40
Buzzer__PORT EQU 2
Buzzer__PRT EQU CYREG_PRT2_PRT
Buzzer__PRTDSI__CAPS_SEL EQU CYREG_PRT2_CAPS_SEL
Buzzer__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT2_DBL_SYNC_IN
Buzzer__PRTDSI__OE_SEL0 EQU CYREG_PRT2_OE_SEL0
Buzzer__PRTDSI__OE_SEL1 EQU CYREG_PRT2_OE_SEL1
Buzzer__PRTDSI__OUT_SEL0 EQU CYREG_PRT2_OUT_SEL0
Buzzer__PRTDSI__OUT_SEL1 EQU CYREG_PRT2_OUT_SEL1
Buzzer__PRTDSI__SYNC_OUT EQU CYREG_PRT2_SYNC_OUT
Buzzer__PS EQU CYREG_PRT2_PS
Buzzer__SHIFT EQU 6
Buzzer__SLW EQU CYREG_PRT2_SLW

/* Blue_LED_pin */
Blue_LED_pin__0__INTTYPE EQU CYREG_PICU2_INTTYPE2
Blue_LED_pin__0__MASK EQU 0x04
Blue_LED_pin__0__PC EQU CYREG_PRT2_PC2
Blue_LED_pin__0__PORT EQU 2
Blue_LED_pin__0__SHIFT EQU 2
Blue_LED_pin__AG EQU CYREG_PRT2_AG
Blue_LED_pin__AMUX EQU CYREG_PRT2_AMUX
Blue_LED_pin__BIE EQU CYREG_PRT2_BIE
Blue_LED_pin__BIT_MASK EQU CYREG_PRT2_BIT_MASK
Blue_LED_pin__BYP EQU CYREG_PRT2_BYP
Blue_LED_pin__CTL EQU CYREG_PRT2_CTL
Blue_LED_pin__DM0 EQU CYREG_PRT2_DM0
Blue_LED_pin__DM1 EQU CYREG_PRT2_DM1
Blue_LED_pin__DM2 EQU CYREG_PRT2_DM2
Blue_LED_pin__DR EQU CYREG_PRT2_DR
Blue_LED_pin__INP_DIS EQU CYREG_PRT2_INP_DIS
Blue_LED_pin__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU2_BASE
Blue_LED_pin__LCD_COM_SEG EQU CYREG_PRT2_LCD_COM_SEG
Blue_LED_pin__LCD_EN EQU CYREG_PRT2_LCD_EN
Blue_LED_pin__MASK EQU 0x04
Blue_LED_pin__PORT EQU 2
Blue_LED_pin__PRT EQU CYREG_PRT2_PRT
Blue_LED_pin__PRTDSI__CAPS_SEL EQU CYREG_PRT2_CAPS_SEL
Blue_LED_pin__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT2_DBL_SYNC_IN
Blue_LED_pin__PRTDSI__OE_SEL0 EQU CYREG_PRT2_OE_SEL0
Blue_LED_pin__PRTDSI__OE_SEL1 EQU CYREG_PRT2_OE_SEL1
Blue_LED_pin__PRTDSI__OUT_SEL0 EQU CYREG_PRT2_OUT_SEL0
Blue_LED_pin__PRTDSI__OUT_SEL1 EQU CYREG_PRT2_OUT_SEL1
Blue_LED_pin__PRTDSI__SYNC_OUT EQU CYREG_PRT2_SYNC_OUT
Blue_LED_pin__PS EQU CYREG_PRT2_PS
Blue_LED_pin__SHIFT EQU 2
Blue_LED_pin__SLW EQU CYREG_PRT2_SLW

/* Red1_LED_pin */
Red1_LED_pin__0__INTTYPE EQU CYREG_PICU2_INTTYPE4
Red1_LED_pin__0__MASK EQU 0x10
Red1_LED_pin__0__PC EQU CYREG_PRT2_PC4
Red1_LED_pin__0__PORT EQU 2
Red1_LED_pin__0__SHIFT EQU 4
Red1_LED_pin__AG EQU CYREG_PRT2_AG
Red1_LED_pin__AMUX EQU CYREG_PRT2_AMUX
Red1_LED_pin__BIE EQU CYREG_PRT2_BIE
Red1_LED_pin__BIT_MASK EQU CYREG_PRT2_BIT_MASK
Red1_LED_pin__BYP EQU CYREG_PRT2_BYP
Red1_LED_pin__CTL EQU CYREG_PRT2_CTL
Red1_LED_pin__DM0 EQU CYREG_PRT2_DM0
Red1_LED_pin__DM1 EQU CYREG_PRT2_DM1
Red1_LED_pin__DM2 EQU CYREG_PRT2_DM2
Red1_LED_pin__DR EQU CYREG_PRT2_DR
Red1_LED_pin__INP_DIS EQU CYREG_PRT2_INP_DIS
Red1_LED_pin__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU2_BASE
Red1_LED_pin__LCD_COM_SEG EQU CYREG_PRT2_LCD_COM_SEG
Red1_LED_pin__LCD_EN EQU CYREG_PRT2_LCD_EN
Red1_LED_pin__MASK EQU 0x10
Red1_LED_pin__PORT EQU 2
Red1_LED_pin__PRT EQU CYREG_PRT2_PRT
Red1_LED_pin__PRTDSI__CAPS_SEL EQU CYREG_PRT2_CAPS_SEL
Red1_LED_pin__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT2_DBL_SYNC_IN
Red1_LED_pin__PRTDSI__OE_SEL0 EQU CYREG_PRT2_OE_SEL0
Red1_LED_pin__PRTDSI__OE_SEL1 EQU CYREG_PRT2_OE_SEL1
Red1_LED_pin__PRTDSI__OUT_SEL0 EQU CYREG_PRT2_OUT_SEL0
Red1_LED_pin__PRTDSI__OUT_SEL1 EQU CYREG_PRT2_OUT_SEL1
Red1_LED_pin__PRTDSI__SYNC_OUT EQU CYREG_PRT2_SYNC_OUT
Red1_LED_pin__PS EQU CYREG_PRT2_PS
Red1_LED_pin__SHIFT EQU 4
Red1_LED_pin__SLW EQU CYREG_PRT2_SLW

/* Red2_LED_pin */
Red2_LED_pin__0__INTTYPE EQU CYREG_PICU2_INTTYPE5
Red2_LED_pin__0__MASK EQU 0x20
Red2_LED_pin__0__PC EQU CYREG_PRT2_PC5
Red2_LED_pin__0__PORT EQU 2
Red2_LED_pin__0__SHIFT EQU 5
Red2_LED_pin__AG EQU CYREG_PRT2_AG
Red2_LED_pin__AMUX EQU CYREG_PRT2_AMUX
Red2_LED_pin__BIE EQU CYREG_PRT2_BIE
Red2_LED_pin__BIT_MASK EQU CYREG_PRT2_BIT_MASK
Red2_LED_pin__BYP EQU CYREG_PRT2_BYP
Red2_LED_pin__CTL EQU CYREG_PRT2_CTL
Red2_LED_pin__DM0 EQU CYREG_PRT2_DM0
Red2_LED_pin__DM1 EQU CYREG_PRT2_DM1
Red2_LED_pin__DM2 EQU CYREG_PRT2_DM2
Red2_LED_pin__DR EQU CYREG_PRT2_DR
Red2_LED_pin__INP_DIS EQU CYREG_PRT2_INP_DIS
Red2_LED_pin__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU2_BASE
Red2_LED_pin__LCD_COM_SEG EQU CYREG_PRT2_LCD_COM_SEG
Red2_LED_pin__LCD_EN EQU CYREG_PRT2_LCD_EN
Red2_LED_pin__MASK EQU 0x20
Red2_LED_pin__PORT EQU 2
Red2_LED_pin__PRT EQU CYREG_PRT2_PRT
Red2_LED_pin__PRTDSI__CAPS_SEL EQU CYREG_PRT2_CAPS_SEL
Red2_LED_pin__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT2_DBL_SYNC_IN
Red2_LED_pin__PRTDSI__OE_SEL0 EQU CYREG_PRT2_OE_SEL0
Red2_LED_pin__PRTDSI__OE_SEL1 EQU CYREG_PRT2_OE_SEL1
Red2_LED_pin__PRTDSI__OUT_SEL0 EQU CYREG_PRT2_OUT_SEL0
Red2_LED_pin__PRTDSI__OUT_SEL1 EQU CYREG_PRT2_OUT_SEL1
Red2_LED_pin__PRTDSI__SYNC_OUT EQU CYREG_PRT2_SYNC_OUT
Red2_LED_pin__PS EQU CYREG_PRT2_PS
Red2_LED_pin__SHIFT EQU 5
Red2_LED_pin__SLW EQU CYREG_PRT2_SLW

/* Green_LED_pin */
Green_LED_pin__0__INTTYPE EQU CYREG_PICU2_INTTYPE3
Green_LED_pin__0__MASK EQU 0x08
Green_LED_pin__0__PC EQU CYREG_PRT2_PC3
Green_LED_pin__0__PORT EQU 2
Green_LED_pin__0__SHIFT EQU 3
Green_LED_pin__AG EQU CYREG_PRT2_AG
Green_LED_pin__AMUX EQU CYREG_PRT2_AMUX
Green_LED_pin__BIE EQU CYREG_PRT2_BIE
Green_LED_pin__BIT_MASK EQU CYREG_PRT2_BIT_MASK
Green_LED_pin__BYP EQU CYREG_PRT2_BYP
Green_LED_pin__CTL EQU CYREG_PRT2_CTL
Green_LED_pin__DM0 EQU CYREG_PRT2_DM0
Green_LED_pin__DM1 EQU CYREG_PRT2_DM1
Green_LED_pin__DM2 EQU CYREG_PRT2_DM2
Green_LED_pin__DR EQU CYREG_PRT2_DR
Green_LED_pin__INP_DIS EQU CYREG_PRT2_INP_DIS
Green_LED_pin__INTTYPE_BASE EQU CYDEV_PICU_INTTYPE_PICU2_BASE
Green_LED_pin__LCD_COM_SEG EQU CYREG_PRT2_LCD_COM_SEG
Green_LED_pin__LCD_EN EQU CYREG_PRT2_LCD_EN
Green_LED_pin__MASK EQU 0x08
Green_LED_pin__PORT EQU 2
Green_LED_pin__PRT EQU CYREG_PRT2_PRT
Green_LED_pin__PRTDSI__CAPS_SEL EQU CYREG_PRT2_CAPS_SEL
Green_LED_pin__PRTDSI__DBL_SYNC_IN EQU CYREG_PRT2_DBL_SYNC_IN
Green_LED_pin__PRTDSI__OE_SEL0 EQU CYREG_PRT2_OE_SEL0
Green_LED_pin__PRTDSI__OE_SEL1 EQU CYREG_PRT2_OE_SEL1
Green_LED_pin__PRTDSI__OUT_SEL0 EQU CYREG_PRT2_OUT_SEL0
Green_LED_pin__PRTDSI__OUT_SEL1 EQU CYREG_PRT2_OUT_SEL1
Green_LED_pin__PRTDSI__SYNC_OUT EQU CYREG_PRT2_SYNC_OUT
Green_LED_pin__PS EQU CYREG_PRT2_PS
Green_LED_pin__SHIFT EQU 3
Green_LED_pin__SLW EQU CYREG_PRT2_SLW

/* Miscellaneous */
BCLK__BUS_CLK__HZ EQU 24000000
BCLK__BUS_CLK__KHZ EQU 24000
BCLK__BUS_CLK__MHZ EQU 24
CYDEV_CHIP_DIE_LEOPARD EQU 1
CYDEV_CHIP_DIE_PANTHER EQU 18
CYDEV_CHIP_DIE_PSOC4A EQU 10
CYDEV_CHIP_DIE_PSOC5LP EQU 17
CYDEV_CHIP_DIE_TMA4 EQU 2
CYDEV_CHIP_DIE_UNKNOWN EQU 0
CYDEV_CHIP_FAMILY_PSOC3 EQU 1
CYDEV_CHIP_FAMILY_PSOC4 EQU 2
CYDEV_CHIP_FAMILY_PSOC5 EQU 3
CYDEV_CHIP_FAMILY_UNKNOWN EQU 0
CYDEV_CHIP_FAMILY_USED EQU CYDEV_CHIP_FAMILY_PSOC5
CYDEV_CHIP_JTAG_ID EQU 0x2E160069
CYDEV_CHIP_MEMBER_3A EQU 1
CYDEV_CHIP_MEMBER_4A EQU 10
CYDEV_CHIP_MEMBER_4C EQU 15
CYDEV_CHIP_MEMBER_4D EQU 6
CYDEV_CHIP_MEMBER_4E EQU 4
CYDEV_CHIP_MEMBER_4F EQU 11
CYDEV_CHIP_MEMBER_4G EQU 2
CYDEV_CHIP_MEMBER_4H EQU 9
CYDEV_CHIP_MEMBER_4I EQU 14
CYDEV_CHIP_MEMBER_4J EQU 7
CYDEV_CHIP_MEMBER_4K EQU 8
CYDEV_CHIP_MEMBER_4L EQU 13
CYDEV_CHIP_MEMBER_4M EQU 12
CYDEV_CHIP_MEMBER_4N EQU 5
CYDEV_CHIP_MEMBER_4U EQU 3
CYDEV_CHIP_MEMBER_5A EQU 17
CYDEV_CHIP_MEMBER_5B EQU 16
CYDEV_CHIP_MEMBER_UNKNOWN EQU 0
CYDEV_CHIP_MEMBER_USED EQU CYDEV_CHIP_MEMBER_5B
CYDEV_CHIP_DIE_EXPECT EQU CYDEV_CHIP_MEMBER_USED
CYDEV_CHIP_DIE_ACTUAL EQU CYDEV_CHIP_DIE_EXPECT
CYDEV_CHIP_REV_LEOPARD_ES1 EQU 0
CYDEV_CHIP_REV_LEOPARD_ES2 EQU 1
CYDEV_CHIP_REV_LEOPARD_ES3 EQU 3
CYDEV_CHIP_REV_LEOPARD_PRODUCTION EQU 3
CYDEV_CHIP_REV_PANTHER_ES0 EQU 0
CYDEV_CHIP_REV_PANTHER_ES1 EQU 1
CYDEV_CHIP_REV_PANTHER_PRODUCTION EQU 1
CYDEV_CHIP_REV_PSOC4A_ES0 EQU 17
CYDEV_CHIP_REV_PSOC4A_PRODUCTION EQU 17
CYDEV_CHIP_REV_PSOC5LP_ES0 EQU 0
CYDEV_CHIP_REV_PSOC5LP_PRODUCTION EQU 0
CYDEV_CHIP_REV_TMA4_ES EQU 17
CYDEV_CHIP_REV_TMA4_ES2 EQU 33
CYDEV_CHIP_REV_TMA4_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_3A_ES1 EQU 0
CYDEV_CHIP_REVISION_3A_ES2 EQU 1
CYDEV_CHIP_REVISION_3A_ES3 EQU 3
CYDEV_CHIP_REVISION_3A_PRODUCTION EQU 3
CYDEV_CHIP_REVISION_4A_ES0 EQU 17
CYDEV_CHIP_REVISION_4A_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_4C_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4D_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4E_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION_256DMA EQU 0
CYDEV_CHIP_REVISION_4F_PRODUCTION_256K EQU 0
CYDEV_CHIP_REVISION_4G_ES EQU 17
CYDEV_CHIP_REVISION_4G_ES2 EQU 33
CYDEV_CHIP_REVISION_4G_PRODUCTION EQU 17
CYDEV_CHIP_REVISION_4H_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4I_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4J_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4K_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4L_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4M_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4N_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_4U_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_5A_ES0 EQU 0
CYDEV_CHIP_REVISION_5A_ES1 EQU 1
CYDEV_CHIP_REVISION_5A_PRODUCTION EQU 1
CYDEV_CHIP_REVISION_5B_ES0 EQU 0
CYDEV_CHIP_REVISION_5B_PRODUCTION EQU 0
CYDEV_CHIP_REVISION_USED EQU CYDEV_CHIP_REVISION_5B_PRODUCTION
CYDEV_CHIP_REV_EXPECT EQU CYDEV_CHIP_REVISION_USED
CYDEV_CONFIG_FASTBOOT_ENABLED EQU 1
CYDEV_CONFIG_UNUSED_IO_AllowButWarn EQU 0
CYDEV_CONFIG_UNUSED_IO EQU CYDEV_CONFIG_UNUSED_IO_AllowButWarn
CYDEV_CONFIG_UNUSED_IO_AllowWithInfo EQU 1
CYDEV_CONFIG_UNUSED_IO_Disallowed EQU 2
CYDEV_CONFIGURATION_COMPRESSED EQU 1
CYDEV_CONFIGURATION_DMA EQU 0
CYDEV_CONFIGURATION_ECC EQU 1
CYDEV_CONFIGURATION_IMOENABLED EQU CYDEV_CONFIG_FASTBOOT_ENABLED
CYDEV_CONFIGURATION_MODE_COMPRESSED EQU 0
CYDEV_CONFIGURATION_MODE EQU CYDEV_CONFIGURATION_MODE_COMPRESSED
CYDEV_CONFIGURATION_MODE_DMA EQU 2
CYDEV_CONFIGURATION_MODE_UNCOMPRESSED EQU 1
CYDEV_DEBUG_ENABLE_MASK EQU 0x20
CYDEV_DEBUG_ENABLE_REGISTER EQU CYREG_MLOGIC_DEBUG
CYDEV_DEBUGGING_DPS_Disable EQU 3
CYDEV_DEBUGGING_DPS_JTAG_4 EQU 1
CYDEV_DEBUGGING_DPS_JTAG_5 EQU 0
CYDEV_DEBUGGING_DPS_SWD EQU 2
CYDEV_DEBUGGING_DPS_SWD_SWV EQU 6
CYDEV_DEBUGGING_DPS EQU CYDEV_DEBUGGING_DPS_SWD_SWV
CYDEV_DEBUGGING_ENABLE EQU 1
CYDEV_DEBUGGING_XRES EQU 0
CYDEV_DMA_CHANNELS_AVAILABLE EQU 24
CYDEV_ECC_ENABLE EQU 0
CYDEV_HEAP_SIZE EQU 0x80
CYDEV_INSTRUCT_CACHE_ENABLED EQU 1
CYDEV_INTR_RISING EQU 0x00000000
CYDEV_IS_EXPORTING_CODE EQU 0
CYDEV_IS_IMPORTING_CODE EQU 0
CYDEV_PROJ_TYPE EQU 0
CYDEV_PROJ_TYPE_BOOTLOADER EQU 1
CYDEV_PROJ_TYPE_LAUNCHER EQU 5
CYDEV_PROJ_TYPE_LOADABLE EQU 2
CYDEV_PROJ_TYPE_LOADABLEANDBOOTLOADER EQU 4
CYDEV_PROJ_TYPE_MULTIAPPBOOTLOADER EQU 3
CYDEV_PROJ_TYPE_STANDARD EQU 0
CYDEV_PROTECTION_ENABLE EQU 0
CYDEV_STACK_SIZE EQU 0x200
CYDEV_USE_BUNDLED_CMSIS EQU 1
CYDEV_VARIABLE_VDDA EQU 0
CYDEV_VDDA_MV EQU 5000
CYDEV_VDDD_MV EQU 5000
CYDEV_VDDIO0_MV EQU 5000
CYDEV_VDDIO1_MV EQU 5000
CYDEV_VDDIO2_MV EQU 5000
CYDEV_VDDIO3_MV EQU 5000
CYDEV_VIO0_MV EQU 5000
CYDEV_VIO1_MV EQU 5000
CYDEV_VIO2_MV EQU 5000
CYDEV_VIO3_MV EQU 5000
CYIPBLOCK_ARM_CM3_VERSION EQU 0
CYIPBLOCK_P3_ANAIF_VERSION EQU 0
CYIPBLOCK_P3_CAN_VERSION EQU 0
CYIPBLOCK_P3_CAPSENSE_VERSION EQU 0
CYIPBLOCK_P3_COMP_VERSION EQU 0
CYIPBLOCK_P3_DECIMATOR_VERSION EQU 0
CYIPBLOCK_P3_DFB_VERSION EQU 0
CYIPBLOCK_P3_DMA_VERSION EQU 0
CYIPBLOCK_P3_DRQ_VERSION EQU 0
CYIPBLOCK_P3_DSM_VERSION EQU 0
CYIPBLOCK_P3_EMIF_VERSION EQU 0
CYIPBLOCK_P3_I2C_VERSION EQU 0
CYIPBLOCK_P3_LCD_VERSION EQU 0
CYIPBLOCK_P3_LPF_VERSION EQU 0
CYIPBLOCK_P3_OPAMP_VERSION EQU 0
CYIPBLOCK_P3_PM_VERSION EQU 0
CYIPBLOCK_P3_SCCT_VERSION EQU 0
CYIPBLOCK_P3_TIMER_VERSION EQU 0
CYIPBLOCK_P3_USB_VERSION EQU 0
CYIPBLOCK_P3_VIDAC_VERSION EQU 0
CYIPBLOCK_P3_VREF_VERSION EQU 0
CYIPBLOCK_S8_GPIO_VERSION EQU 0
CYIPBLOCK_S8_IRQ_VERSION EQU 0
CYIPBLOCK_S8_SAR_VERSION EQU 0
CYIPBLOCK_S8_SIO_VERSION EQU 0
CYIPBLOCK_S8_UDB_VERSION EQU 0
DMA_CHANNELS_USED__MASK0 EQU 0x00000000
CYDEV_BOOTLOADER_ENABLE EQU 0

#endif /* INCLUDED_CYFITTERIAR_INC */
