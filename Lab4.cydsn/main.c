
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"
#include "queue.h"


#define FOREVER (1 == 1)

/* Blink task parameters */
#define BLINK_STACK_SIZE 64
#define BUTTONS_PRIORITY 5
#define BLUE_LED_PRIORITY 4
#define GREEN_LED_PRIORITY 3
#define RED1_LED_PRIORITY 2


static void Blue_LED_Task(void *parm);
static void Green_LED_Task(void *parm);
static void Red1_LED_Task(void *parm);
static void Check_Buttons_Task(void *parm);


struct BTNS {
    unsigned prevState : 1;
    unsigned curState : 1;
};

struct BTNS sw1, sw2, sw3;


#define PRESSED 1
#define NOT_PRESSED 0

/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}


int main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();

    
    /* Create the tasks */
  	( void ) xTaskCreate( Blue_LED_Task, "BlueLED", BLINK_STACK_SIZE, NULL, BLUE_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Green_LED_Task, "GreenLED", BLINK_STACK_SIZE, NULL, GREEN_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Red1_LED_Task, "Red1LED", BLINK_STACK_SIZE, NULL, RED1_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Check_Buttons_Task, "Buttons", BLINK_STACK_SIZE, NULL, BUTTONS_PRIORITY, NULL );
    
    

    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/

static void Blue_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        if(sw1.curState == PRESSED){
            Blue_LED_pin_Write(1);
        }else{
            Blue_LED_pin_Write(0);
        }
        vTaskDelay( 50 );
    }
}

static void Green_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {  
        if(sw2.curState == PRESSED){
            Green_LED_pin_Write(1);
        }else{
            Green_LED_pin_Write(0);
        }
        vTaskDelay( 50 );
    }
}

static void Red1_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        if(sw3.curState == PRESSED){
            Red1_LED_pin_Write(1);
        }else{
            Red1_LED_pin_Write(0);
        }
        vTaskDelay( 50 );
    }
}


static void Check_Buttons_Task(void *parm){
    
    (void) parm;
    
    while(FOREVER){
        if(sw1_Read() == 0){
            sw1.curState = PRESSED;
        }else{
            sw1.curState = NOT_PRESSED;
        }
        if(sw2_Read() == 0){
            sw2.curState = PRESSED;
        }else{
            sw2.curState = NOT_PRESSED;
        }
        if(sw3_Read() == 0){
            sw3.curState = PRESSED;
        }else{
            sw3.curState = NOT_PRESSED;
        }
        
        vTaskDelay( 50 );
    }
}