
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"
#include "math.h"
#include "stdlib.h"
#include "complex.h"
#include "TM1636.h"

//-Declare the resistance of the resistor that connects with the temperature sensor-//
#define RESISTOR_CONNECT_THERMISTOR	10000//the resistance is 8k ohm

#define FOREVER (1 == 1)

/* Blink task parameters */
#define BLINK_STACK_SIZE 64

int16_t lightoutput;
int8_t mute = 0;

static void Check_Buttons_Task(void *parm);
static void Update_Display_Task(void *parm);
static void Light_Sensor_Task (void *parm);
static void Alarm_Task(void *parm);


/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}


int main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();
       
    /* Start ADC, AMux, OpAmp components */
	ADC_DelSig_1_Start();
    
    /* Start the ADC conversion */
    ADC_DelSig_1_StartConvert();
    
    /*Initialize Display*/
    TM1636init();
    
    /* Create the tasks */
    ( void ) xTaskCreate( Check_Buttons_Task, "Buttons", BLINK_STACK_SIZE, NULL, 1, NULL );
    ( void ) xTaskCreate( Update_Display_Task, "Display", BLINK_STACK_SIZE, NULL, 1, NULL );
    ( void ) xTaskCreate( Light_Sensor_Task, "Sensor", BLINK_STACK_SIZE, NULL, 1, NULL );
    ( void ) xTaskCreate( Alarm_Task, "Alarm", BLINK_STACK_SIZE, NULL, 1, NULL );
    
    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/

static void Check_Buttons_Task(void *parm){
    (void) parm;
    
    while(FOREVER){
        if(sw1_Read() == 0){
            mute = !mute;
            while(sw1_Read() == 0);
        }
        vTaskDelay( 50 );
    }
}

static void Update_Display_Task(void *parm){
    (void) parm;
      
    while( FOREVER ){
        
        int8_t temp[4];

        if(lightoutput < 1000)
        {
        	temp[0] = INDEX_BLANK;
        }
        else
        {
        	temp[0] = lightoutput/1000;
        }
        
        lightoutput %= 1000;
        temp[1] = lightoutput / 100; //index of second bit from left
        temp[2] = (lightoutput/10) % (temp[1] * 10);//index of third bit from left
        temp[3] = lightoutput % 10;//index of the fourth bit from left
        TM1636_Full_Screen(temp);
        vTaskDelay( 50 );
    }
}

static void Light_Sensor_Task (void *parm){
    (void) parm;
    
    while( FOREVER ){
        if(ADC_DelSig_1_IsEndConversion(ADC_DelSig_1_RETURN_STATUS))
        {
            lightoutput = ADC_DelSig_1_GetResult16();
        }
        vTaskDelay( 50 );
    }
}

static void Alarm_Task(void *parm){
    (void) parm;
    while( FOREVER ){
        if(lightoutput > 600){
            if(mute == 0){
                Buzzer_Write(1);
            }else{
                Buzzer_Write(0);
                Red1_LED_pin_Write(!Red1_LED_pin_Read());
            }
        }else{
            Buzzer_Write(0);
            Red1_LED_pin_Write(0);
            mute = 0;
        }
        vTaskDelay( 500 );
    }
}