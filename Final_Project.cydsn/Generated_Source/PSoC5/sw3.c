/*******************************************************************************
* File Name: sw3.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "sw3.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 sw3__PORT == 15 && ((sw3__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: sw3_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void sw3_Write(uint8 value) 
{
    uint8 staticBits = (sw3_DR & (uint8)(~sw3_MASK));
    sw3_DR = staticBits | ((uint8)(value << sw3_SHIFT) & sw3_MASK);
}


/*******************************************************************************
* Function Name: sw3_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  sw3_DM_STRONG     Strong Drive 
*  sw3_DM_OD_HI      Open Drain, Drives High 
*  sw3_DM_OD_LO      Open Drain, Drives Low 
*  sw3_DM_RES_UP     Resistive Pull Up 
*  sw3_DM_RES_DWN    Resistive Pull Down 
*  sw3_DM_RES_UPDWN  Resistive Pull Up/Down 
*  sw3_DM_DIG_HIZ    High Impedance Digital 
*  sw3_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void sw3_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(sw3_0, mode);
}


/*******************************************************************************
* Function Name: sw3_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro sw3_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 sw3_Read(void) 
{
    return (sw3_PS & sw3_MASK) >> sw3_SHIFT;
}


/*******************************************************************************
* Function Name: sw3_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 sw3_ReadDataReg(void) 
{
    return (sw3_DR & sw3_MASK) >> sw3_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(sw3_INTSTAT) 

    /*******************************************************************************
    * Function Name: sw3_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 sw3_ClearInterrupt(void) 
    {
        return (sw3_INTSTAT & sw3_MASK) >> sw3_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
