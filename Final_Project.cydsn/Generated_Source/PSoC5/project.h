/*******************************************************************************
* File Name: project.h
* 
* PSoC Creator  3.3 CP1
*
* Description:
* It contains references to all generated header files and should not be modified.
* This file is automatically generated by PSoC Creator.
*
********************************************************************************
* Copyright (c) 2007-2015 Cypress Semiconductor.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cyfitter_cfg.h"
#include "cydevice.h"
#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cydisabledsheets.h"
#include "Blue_LED_pin_aliases.h"
#include "Blue_LED_pin.h"
#include "Green_LED_pin_aliases.h"
#include "Green_LED_pin.h"
#include "Red1_LED_pin_aliases.h"
#include "Red1_LED_pin.h"
#include "Red2_LED_pin_aliases.h"
#include "Red2_LED_pin.h"
#include "sw1_aliases.h"
#include "sw1.h"
#include "sw2_aliases.h"
#include "sw2.h"
#include "sw3_aliases.h"
#include "sw3.h"
#include "Buzzer_aliases.h"
#include "Buzzer.h"
#include "Data_aliases.h"
#include "Data.h"
#include "Clock_aliases.h"
#include "Clock.h"
#include "Temp_sensor_aliases.h"
#include "Temp_sensor.h"
#include "ADC_DelSig_1.h"
#include "ADC_DelSig_1_AMux.h"
#include "ADC_DelSig_1_Ext_CP_Clk.h"
#include "ADC_DelSig_1_IRQ.h"
#include "ADC_DelSig_1_theACLK.h"
#include "core_cm3_psoc5.h"
#include "core_cm3.h"
#include "CyDmac.h"
#include "CyFlash.h"
#include "CyLib.h"
#include "cypins.h"
#include "cyPm.h"
#include "CySpc.h"
#include "cytypes.h"
#include "core_cmFunc.h"
#include "core_cmInstr.h"

/*[]*/

