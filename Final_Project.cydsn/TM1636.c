/* ========================================
*
* Copyright YOUR COMPANY, THE YEAR
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF your company.
*
* ========================================
*/
//  Author:Frankie.Chu
//  Date:9 April,2012
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//
/*******************************************************************************/
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"
#include "TM1636.h"

static int8_t TubeTab[] = {0x3f,0x06,0x5b,0x4f,
                       0x66,0x6d,0x7d,0x07,
                       0x7f,0x6f,0x77,0x7c,
                       0x39,0x5e,0x79,0x71,
                       0x40,0x00};//0~9,A,b,C,d,E,F,"-"," "                        

/*Set the default brightness is BRIGHT_TYPICAL and clear the display*/
void TM1636init(void)
{
    TM1636set(BRIGHTEST, 0x40, 0xC0);
    TM1636clearDisplay();
    Data_SetDriveMode(Data_DM_STRONG);
}

void TM1636writeByte(int8_t wr_data)
{
    uint8_t i,count1 = 0;   
    for(i=0;i<8;i++)        //sent 8bit data
    {
        Clock_Write(0);     
        if(wr_data & 0x01)Data_Write(1);//LSB first
        else Data_Write(0);
        wr_data >>= 1;      
        Clock_Write(1);
    }  
    Clock_Write(0);  //wait for the ACK
    Data_Write(1);
    Clock_Write(1);     
    //pinMode(Datapin,INPUT);
    Data_SetDriveMode(Data_DM_DIG_HIZ);
    while(Data_Read())    
    { 
        count1 +=1;
        if(count1 == 200)//
        {
         Data_SetDriveMode(Data_DM_STRONG);
         //pinMode(Datapin,OUTPUT);
         Data_Write(0);
         count1 =0;
        }
        Data_SetDriveMode(Data_DM_DIG_HIZ);
        //pinMode(Datapin,INPUT);
    }
    Data_SetDriveMode(Data_DM_STRONG);
   // pinMode(Datapin,OUTPUT);
}
//send start signal to TM1636
void TM1636start(void)
{
    Clock_Write(1);//send start signal to TM1637
    Data_Write(1); 
    Data_Write(0); 
    Clock_Write(0); 
} 
//End of transmission
void TM1636stop(void)
{
    Clock_Write(0);
    Data_Write(0);
    Clock_Write(1);
    Data_Write(1); 
}
//display function.Write to full-screen.
void TM1636_Full_Screen(int8_t DispData[])
{
    int8_t SegData[4];
    uint8_t i;
    for(i = 0;i < 4;i ++)
    {
        SegData[i] = DispData[i];
    }
    TM1636coding(SegData);
    TM1636start();          //start signal sent to TM1637 from MCU
    TM1636writeByte(ADDR_AUTO);//
    TM1636stop();           //
    TM1636start();          //
    TM1636writeByte(Cmd_SetAddr);//
    for(i=0;i < 4;i ++)
    {
        TM1636writeByte(SegData[i]);        //
    }
    TM1636stop();           //
    TM1636start();          //
    TM1636writeByte(Cmd_DispCtrl);//
    TM1636stop();           //
}
//******************************************
void TM1636display(uint8_t BitAddr,int8_t DispData)
{
    int8_t SegData;
    SegData = TM1636_coding(DispData);
    TM1636start();          //start signal sent to TM1637 from MCU
    TM1636writeByte(ADDR_FIXED);//
    TM1636stop();           //
    TM1636start();          //
    TM1636writeByte(BitAddr|0xc0);//
    TM1636writeByte(SegData);//
    TM1636stop();            //
    TM1636start();          //
    TM1636writeByte(Cmd_DispCtrl);//
    TM1636stop();           //
}

void TM1636clearDisplay(void)
{
    TM1636display(0x00,0x7f);
    TM1636display(0x01,0x7f);
    TM1636display(0x02,0x7f);
    TM1636display(0x03,0x7f);  
}
//To take effect the next time it displays.
void TM1636set(uint8_t brightness,uint8_t SetData,uint8_t SetAddr)
{
    DisplayBrigtness = brightness;
    Cmd_SetData = SetData;
    Cmd_SetAddr = SetAddr;
    Cmd_DispCtrl = 0x88 + brightness;//Set the brightness and it takes effect the next time it displays.
}

//Whether to light the clock point ":".
//To take effect the next time it displays.
void TM1636point(uint8_t PointFlag)
{
    DisplayPointFlag = PointFlag;
}

void TM1636coding(int8_t DispData[])
{
    uint8_t  i;
    uint8_t PointData;
    if(DisplayPointFlag == POINT_ON)PointData = 0x80;
    else PointData = 0; 
    for(i = 0;i < 4;i ++)
    {
        if(DispData[i] == 0x7f)DispData[i] = 0x00;
        else DispData[i] = TubeTab[DispData[i]] + PointData;
    }
}

int8_t TM1636_coding(int8_t DispData)
{
    uint8_t PointData;
    if(DisplayPointFlag == POINT_ON)PointData = 0x80;
    else PointData = 0; 
    if(DispData == 0x7f) DispData = 0x00 + PointData;//The bit digital tube off
    else DispData = TubeTab[DispData] + PointData;
    return DispData;
}

/* [] END OF FILE */
