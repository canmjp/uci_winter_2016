/* ========================================
*
* Copyright YOUR COMPANY, THE YEAR
* All Rights Reserved
* UNPUBLISHED, LICENSED SOFTWARE.
*
* CONFIDENTIAL AND PROPRIETARY INFORMATION
* WHICH IS THE PROPERTY OF your company.
*
* ========================================
*/
//  Author:Frankie.Chu
//  Date:9 April,2012
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//
/*******************************************************************************/

#ifndef TM1636_h
#define TM1636_h
#include <inttypes.h>

//************definitions for TM1636*********************
#define ADDR_AUTO  0x40
#define ADDR_FIXED 0x44

#define STARTADDR  0xc0 
/**** definitions for the clock point of the digit tube *******/
#define POINT_ON   1
#define POINT_OFF  0
/**************definitions for brightness***********************/
#define  BRIGHT_DARKEST 0
#define  BRIGHT_TYPICAL 2
#define  BRIGHTEST      7

//--------------------------------------------------------//
//Special characters index of tube table
#define INDEX_NEGATIVE_SIGN	16
#define INDEX_BLANK			17

uint8_t Cmd_SetData;
uint8_t Cmd_SetAddr;
uint8_t Cmd_DispCtrl;
uint8_t DisplayPointFlag;     //_PointFlag=1:the clock point on
uint8_t DisplayBrigtness;
    
void TM1636init(void);        //To clear the display
void TM1636writeByte(int8_t wr_data);//write 8bit data to tm1637
void TM1636start(void);//send start bits
void TM1636stop(void); //send stop bits
void TM1636_Full_Screen(int8_t DispData[]);
void TM1636display(uint8_t BitAddr,int8_t DispData);
void TM1636clearDisplay(void);
void TM1636set(uint8_t,uint8_t,uint8_t);//To take effect the next time it displays.
void TM1636point(uint8_t PointFlag);//whether to light the clock point ":".To take effect the next time it displays.
void TM1636coding(int8_t DispData[]); 
int8_t TM1636_coding(int8_t DispData); 

#endif

/* [] END OF FILE */
