/*******************************************************************************
* File Name: sw2.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_sw2_H) /* Pins sw2_H */
#define CY_PINS_sw2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sw2_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v2_10 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sw2__PORT == 15 && ((sw2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    sw2_Write(uint8 value) ;
void    sw2_SetDriveMode(uint8 mode) ;
uint8   sw2_ReadDataReg(void) ;
uint8   sw2_Read(void) ;
uint8   sw2_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define sw2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define sw2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define sw2_DM_RES_UP          PIN_DM_RES_UP
#define sw2_DM_RES_DWN         PIN_DM_RES_DWN
#define sw2_DM_OD_LO           PIN_DM_OD_LO
#define sw2_DM_OD_HI           PIN_DM_OD_HI
#define sw2_DM_STRONG          PIN_DM_STRONG
#define sw2_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define sw2_MASK               sw2__MASK
#define sw2_SHIFT              sw2__SHIFT
#define sw2_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sw2_PS                     (* (reg8 *) sw2__PS)
/* Data Register */
#define sw2_DR                     (* (reg8 *) sw2__DR)
/* Port Number */
#define sw2_PRT_NUM                (* (reg8 *) sw2__PRT) 
/* Connect to Analog Globals */                                                  
#define sw2_AG                     (* (reg8 *) sw2__AG)                       
/* Analog MUX bux enable */
#define sw2_AMUX                   (* (reg8 *) sw2__AMUX) 
/* Bidirectional Enable */                                                        
#define sw2_BIE                    (* (reg8 *) sw2__BIE)
/* Bit-mask for Aliased Register Access */
#define sw2_BIT_MASK               (* (reg8 *) sw2__BIT_MASK)
/* Bypass Enable */
#define sw2_BYP                    (* (reg8 *) sw2__BYP)
/* Port wide control signals */                                                   
#define sw2_CTL                    (* (reg8 *) sw2__CTL)
/* Drive Modes */
#define sw2_DM0                    (* (reg8 *) sw2__DM0) 
#define sw2_DM1                    (* (reg8 *) sw2__DM1)
#define sw2_DM2                    (* (reg8 *) sw2__DM2) 
/* Input Buffer Disable Override */
#define sw2_INP_DIS                (* (reg8 *) sw2__INP_DIS)
/* LCD Common or Segment Drive */
#define sw2_LCD_COM_SEG            (* (reg8 *) sw2__LCD_COM_SEG)
/* Enable Segment LCD */
#define sw2_LCD_EN                 (* (reg8 *) sw2__LCD_EN)
/* Slew Rate Control */
#define sw2_SLW                    (* (reg8 *) sw2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sw2_PRTDSI__CAPS_SEL       (* (reg8 *) sw2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sw2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sw2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sw2_PRTDSI__OE_SEL0        (* (reg8 *) sw2__PRTDSI__OE_SEL0) 
#define sw2_PRTDSI__OE_SEL1        (* (reg8 *) sw2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sw2_PRTDSI__OUT_SEL0       (* (reg8 *) sw2__PRTDSI__OUT_SEL0) 
#define sw2_PRTDSI__OUT_SEL1       (* (reg8 *) sw2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sw2_PRTDSI__SYNC_OUT       (* (reg8 *) sw2__PRTDSI__SYNC_OUT) 


#if defined(sw2__INTSTAT)  /* Interrupt Registers */

    #define sw2_INTSTAT                (* (reg8 *) sw2__INTSTAT)
    #define sw2_SNAP                   (* (reg8 *) sw2__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_sw2_H */


/* [] END OF FILE */
