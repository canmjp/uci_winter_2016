
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"
#include "queue.h"


#define FOREVER (1 == 1)

static void LED_Task(void *parm); /*receive*/
static void sw1_Task(void *parm); /*send*/
static void sw2_Task(void *parm); /*send*/
static void sw3_Task(void *parm); /*send*/


#define PRESSED 1
#define NOT_PRESSED 0
#define SWITCH1 1
#define SWITCH2 2
#define SWITCH3 3


typedef struct button_Data{
    unsigned char currentState;
    unsigned char source;
} buttonData;

/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}

int main( void )
{
    xQueueHandle xQueue;
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();

    /* Create the queue */
    xQueue = xQueueCreate(5, sizeof (buttonData));
    
    /* Create the tasks */
  	( void ) xTaskCreate( sw1_Task, "sw1_task", 240, (void *) xQueue , 2, NULL );
    ( void ) xTaskCreate( sw2_Task, "sw2_task", 240, (void *) xQueue , 2, NULL );
    ( void ) xTaskCreate( sw3_Task, "sw3_task", 240, (void *) xQueue , 2, NULL );
    ( void ) xTaskCreate( LED_Task, "LED_Task", 240, (void *) xQueue , 1, NULL );
    
    

    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/


static void sw1_Task (void *parm){
    xQueueHandle xQueue;
    buttonData xButtonData;
    
    xQueue = (xQueueHandle) parm;
    
    while( FOREVER ){        
        xButtonData.source = SWITCH1;
        if(sw1_Read() == 0){
            xButtonData.currentState = PRESSED;
        }else{
            xButtonData.currentState = NOT_PRESSED;
        }
        
        xQueueSend(xQueue, &xButtonData, 10);
    }
}

static void sw2_Task (void *parm){
    xQueueHandle xQueue;
    buttonData xButtonData;
    
    xQueue = (xQueueHandle) parm;
    
    while( FOREVER ){        
        xButtonData.source = SWITCH2;
        if(sw2_Read() == 0){
            xButtonData.currentState = PRESSED;
        }else{
            xButtonData.currentState = NOT_PRESSED;
        }
        
        xQueueSend(xQueue, &xButtonData, 10);
    }
}

static void sw3_Task (void *parm){
    xQueueHandle xQueue;
    buttonData xButtonData;
    
    xQueue = (xQueueHandle) parm;
    
    while( FOREVER ){        
        xButtonData.source = SWITCH3;
        if(sw3_Read() == 0){
            xButtonData.currentState = PRESSED;
        }else{
            xButtonData.currentState = NOT_PRESSED;
        }
        
        xQueueSend(xQueue, &xButtonData, 10);
    }
}

static void LED_Task (void *parm){
    xQueueHandle xQueue;
    buttonData xButtonData;
    
    xQueue = (xQueueHandle) parm;
    
    while( FOREVER ){
        if(xQueueReceive( xQueue, &xButtonData, 0 )){
            if(xButtonData.source == SWITCH1){
                if(xButtonData.currentState == PRESSED){
                    Blue_LED_pin_Write(1);
                }else{
                    Blue_LED_pin_Write(0);
                }
            }
            if(xButtonData.source == SWITCH2){
                if(xButtonData.currentState == PRESSED){
                    Green_LED_pin_Write(1);
                }else{
                    Green_LED_pin_Write(0);
                }
            }
            if(xButtonData.source == SWITCH3){
                if(xButtonData.currentState == PRESSED){
                    Red1_LED_pin_Write(1);
                }else{
                    Red1_LED_pin_Write(0);
                }
            }
        }
    }
}
