
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"


#define FOREVER (1 == 1)

/* Blink task parameters */
#define BLINK_STACK_SIZE 64
#define BLINK_PRIORITY 3


static void Blink_LED_Task(void *parm);

/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}

int main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();
    
    /* Create the LED blink task */
  	( void ) xTaskCreate( Blink_LED_Task, "Blink", BLINK_STACK_SIZE, NULL, BLINK_PRIORITY, NULL );

    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/

static void Blink_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        /* Toggle the LED */
        LED_pin_Write( !LED_pin_Read( ));
        vTaskDelay( 1000 );
    }
}
