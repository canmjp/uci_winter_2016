/*******************************************************************************
* File Name: Red2_LED_pin.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Red2_LED_pin_ALIASES_H) /* Pins Red2_LED_pin_ALIASES_H */
#define CY_PINS_Red2_LED_pin_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define Red2_LED_pin_0		(Red2_LED_pin__0__PC)

#endif /* End Pins Red2_LED_pin_ALIASES_H */

/* [] END OF FILE */
