/*******************************************************************************
* File Name: Red2_LED_pin.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "Red2_LED_pin.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 Red2_LED_pin__PORT == 15 && ((Red2_LED_pin__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: Red2_LED_pin_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void Red2_LED_pin_Write(uint8 value) 
{
    uint8 staticBits = (Red2_LED_pin_DR & (uint8)(~Red2_LED_pin_MASK));
    Red2_LED_pin_DR = staticBits | ((uint8)(value << Red2_LED_pin_SHIFT) & Red2_LED_pin_MASK);
}


/*******************************************************************************
* Function Name: Red2_LED_pin_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  Red2_LED_pin_DM_STRONG     Strong Drive 
*  Red2_LED_pin_DM_OD_HI      Open Drain, Drives High 
*  Red2_LED_pin_DM_OD_LO      Open Drain, Drives Low 
*  Red2_LED_pin_DM_RES_UP     Resistive Pull Up 
*  Red2_LED_pin_DM_RES_DWN    Resistive Pull Down 
*  Red2_LED_pin_DM_RES_UPDWN  Resistive Pull Up/Down 
*  Red2_LED_pin_DM_DIG_HIZ    High Impedance Digital 
*  Red2_LED_pin_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void Red2_LED_pin_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(Red2_LED_pin_0, mode);
}


/*******************************************************************************
* Function Name: Red2_LED_pin_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Red2_LED_pin_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Red2_LED_pin_Read(void) 
{
    return (Red2_LED_pin_PS & Red2_LED_pin_MASK) >> Red2_LED_pin_SHIFT;
}


/*******************************************************************************
* Function Name: Red2_LED_pin_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Red2_LED_pin_ReadDataReg(void) 
{
    return (Red2_LED_pin_DR & Red2_LED_pin_MASK) >> Red2_LED_pin_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Red2_LED_pin_INTSTAT) 

    /*******************************************************************************
    * Function Name: Red2_LED_pin_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Red2_LED_pin_ClearInterrupt(void) 
    {
        return (Red2_LED_pin_INTSTAT & Red2_LED_pin_MASK) >> Red2_LED_pin_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
