
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"
#include "semphr.h"


#define FOREVER (1 == 1)

/* Blink task parameters */
#define BLINK_STACK_SIZE 64
#define BUTTONS_PRIORITY 1
#define BLUE_LED_PRIORITY 1


static void Blue_LED_Task(void *parm);
static void Check_Buttons_Task(void *parm);


/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}

SemaphoreHandle_t xSemaphore = NULL;

int main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();
    
    /*create semaphore*/
    xSemaphore = xSemaphoreCreateBinary( );
    
    /* Create the tasks */
  	( void ) xTaskCreate( Blue_LED_Task, "BlueLED", BLINK_STACK_SIZE, NULL, BLUE_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Check_Buttons_Task, "Buttons", BLINK_STACK_SIZE, NULL, BUTTONS_PRIORITY, NULL );
    
    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/


static void Blue_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        if( xSemaphoreTake( xSemaphore, 0 ) ){
            Blue_LED_pin_Write(1);
        }else{
            xSemaphoreGive( xSemaphore );
            Blue_LED_pin_Write(0);
            
        }
        
        vTaskDelay( 1000 );
    }
}


static void Check_Buttons_Task(void *parm){
    
    (void) parm;
    
    while(FOREVER){
        if(sw1_Read() == 0){
            if( xSemaphoreTake( xSemaphore, 0)){
                Green_LED_pin_Write(1);
                Red1_LED_pin_Write(0);
                vTaskDelay(200);
                Green_LED_pin_Write(0);
            }else{
                Green_LED_pin_Write(0);
                Red1_LED_pin_Write(1);
                vTaskDelay(200);
                Red1_LED_pin_Write(0);
            }
            xSemaphoreGive( xSemaphore );
        }
        if(sw2_Read() == 0){
            xSemaphoreTake( xSemaphore, portMAX_DELAY);
            Green_LED_pin_Write(1);
        }
        if(sw3_Read() == 0){
            xSemaphoreGive( xSemaphore );
            Green_LED_pin_Write(0);
        }
        vTaskDelay( 50 );
    }
}