
#include "device.h"
#include "FreeRTOS.h"
#include "port.h"
#include "task.h"


#define FOREVER (1 == 1)

/* Blink task parameters */
#define BLINK_STACK_SIZE 64
#define BLUE_LED_PRIORITY 4
#define GREEN_LED_PRIORITY 3
#define RED1_LED_PRIORITY 2
#define RED2_LED_PRIORITY 1


static void Blue_LED_Task(void *parm);
static void Green_LED_Task(void *parm);
static void Red1_LED_Task(void *parm);
static void Red2_LED_Task(void *parm);

/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/


void vApplicationMallocFailedHook(void)
{
    /* The heap space has been execeeded. */
}

void vApplicationStackOverflowHook(void)
{
    /* The stack space has been overflowed */
}

int main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	prvHardwareSetup();
    
    /* Create the LED blink task */
  	( void ) xTaskCreate( Blue_LED_Task, "BlueLED", BLINK_STACK_SIZE, NULL, BLUE_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Green_LED_Task, "GreenLED", BLINK_STACK_SIZE, NULL, GREEN_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Red1_LED_Task, "Red1LED", BLINK_STACK_SIZE, NULL, RED1_LED_PRIORITY, NULL );
    ( void ) xTaskCreate( Red2_LED_Task, "Red2LED", BLINK_STACK_SIZE, NULL, RED2_LED_PRIORITY, NULL );

    /* Start the OS */
    vTaskStartScheduler();
    
    /*NOTREACHED*/
    for(;;);
}

void prvHardwareSetup( void )
{
    /* Port layer functions that need to be copied into the vector table. */
    extern void xPortPendSVHandler( void );
    extern void xPortSysTickHandler( void );
    extern void vPortSVCHandler( void );
    extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;

	/* Start-up the peripherals. */
}
/*---------------------------------------------------------------------------*/

static void Blue_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        /* Toggle the LED */
        Blue_LED_pin_Write( !Blue_LED_pin_Read( ));
        vTaskDelay( 1000 );
    }
}

static void Green_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        /* Toggle the LED */
        Green_LED_pin_Write( !Green_LED_pin_Read( ));
        vTaskDelay( 500 );
    }
}

static void Red1_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        /* Toggle the LED */
        Red1_LED_pin_Write( !Red1_LED_pin_Read( ));
        vTaskDelay( 250 );
    }
}

static void Red2_LED_Task(void *parm)
{
    (void) parm;

    while( FOREVER )
    {
        /* Toggle the LED */
        Red2_LED_pin_Write( !Red2_LED_pin_Read( ));
        vTaskDelay( 100 );
    }
}